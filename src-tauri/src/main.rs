// Import des bibliothèques nécessaires
use rusqlite::{params, Connection, Result};
use std::fs::File;
use std::io::Write;
use std::path::Path;
use notify_rust::Notification;
use dirs;

fn init_db(conn: &Connection) -> Result<(), rusqlite::Error> {
    // Exécute une requête SQL pour créer une table 'notes' si elle n'existe pas déjà
    conn.execute(
        "CREATE TABLE IF NOT EXISTS notes (
            id INTEGER PRIMARY KEY,
            title TEXT NOT NULL,
            content TEXT NOT NULL
        )",
        [],
    )?;
    println!("Database initialized successfully");
    Ok(())
}

fn create_note(conn: &Connection, title: &str, content: &str) -> Result<(), rusqlite::Error> {
    // Exécute une requête SQL pour insérer une nouvelle note avec un titre et un contenu donnés
    conn.execute(
        "INSERT INTO notes (title, content) VALUES (?1, ?2)",
        params![title, content],
    )?;
    println!("Note created successfully");
    // Envoyer une notification
    Notification::new()
        .summary("New note created")
        .body(&format!("Note with name '{}' was created.", title))
        .show().unwrap();
    Ok(())
}

fn read_notes(conn: &Connection) -> Result<Vec<(i64, String, String)>, rusqlite::Error> {
    // Prépare une requête SQL pour sélectionner toutes les notes de la table 'notes'
    let mut stmt = conn.prepare("SELECT id, title, content FROM notes")?;
    // Exécute la requête et récupère les résultats sous forme d'un itérateur de tuples (id, title, content)
    let note_iter = stmt.query_map([], |row| {
        Ok((
            row.get(0)?,
            row.get(1)?,
            row.get(2)?,
        ))
    })?;
    // Collecte les résultats de l'itérateur dans un vecteur de tuples et le retourne
    let notes: Vec<_> = note_iter.collect::<Result<Vec<_>, _>>()?;
    println!("Notes read successfully");
    Ok(notes)
}

fn update_note(conn: &Connection, id: i64, title: &str, content: &str) -> Result<(), rusqlite::Error> {
    // Exécute une requête SQL pour mettre à jour une note avec un ID donné, en modifiant son titre et son contenu
    conn.execute(
        "UPDATE notes SET title = ?1, content = ?2 WHERE id = ?3",
        params![title, content, id],
    )?;
    println!("Note updated successfully");
    Ok(())
}

fn delete_note(conn: &Connection, id: i64) -> Result<(), rusqlite::Error> {
    // Exécute une requête SQL pour supprimer une note avec un ID donné de la table 'notes'
    conn.execute(
        "DELETE FROM notes WHERE id = ?1",
        params![id],
    )?;
    println!("Note deleted successfully");
    Ok(())
}

fn delete_all_notes(conn: &Connection) -> Result<(), rusqlite::Error> {
    // Exécute une requête SQL pour supprimer toutes les notes de la table 'notes'
    conn.execute("DELETE FROM notes", [])?;
    println!("All notes deleted successfully");
    Ok(())
}

fn export_notes(conn: &Connection) -> Result<(), String> {
    // Récupère toutes les notes depuis la base de données
    let notes = match read_notes(conn) {
        Ok(notes) => notes,
        Err(err) => return Err(err.to_string()),
    };

    // Chemin vers le dossier d'exportation
    let export_dir = Path::new("../export");
    // Crée le dossier d'exportation s'il n'existe pas déjà
    std::fs::create_dir_all(export_dir)
        .map_err(|io_err| format!("Failed to create export directory: {}", io_err))?;

    // Chemin complet du fichier d'exportation
    let file_path = export_dir.join("notes.txt");
    // Crée le fichier dans le dossier d'exportation
    let mut file = File::create(file_path)
        .map_err(|io_err| format!("Failed to create file: {}", io_err))?;

    // Écrit chaque note dans le fichier texte avec son ID, titre et contenu
    for (id, title, content) in notes {
        writeln!(&mut file, "Note ID: {}\nTitle: {}\nContent: {}\n", id, title, content)
            .map_err(|io_err| format!("Error writing to file: {}", io_err))?;
    }

    println!("Notes exported to export/notes.txt"); // Indique le chemin du fichier exporté
    Ok(())
}

// Définition des gestionnaires de commandes pour l'interface Tauri
#[tauri::command]
fn init_db_handler() -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    init_db(&conn).map_err(|err| err.to_string())
}

#[tauri::command]
fn create_note_handler(title: String, content: String) -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    create_note(&conn, &title, &content).map_err(|err| err.to_string())
}

#[tauri::command]
fn read_notes_handler() -> Result<Vec<(i64, String, String)>, String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    read_notes(&conn).map_err(|err| err.to_string())
}

#[tauri::command]
fn update_note_handler(id: i64, title: String, content: String) -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    update_note(&conn, id, &title, &content).map_err(|err| err.to_string())
}

#[tauri::command]
fn delete_note_handler(id: i64) -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    delete_note(&conn, id).map_err(|err| err.to_string())
}

#[tauri::command]
fn export_notes_handler() -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    export_notes(&conn).map_err(|err| err.to_string())
}

#[tauri::command]
fn delete_all_notes_handler() -> Result<(), String> {
    let conn = Connection::open("notes.db").map_err(|err| err.to_string())?;
    delete_all_notes(&conn).map_err(|err| err.to_string())
}

// Fonction principale pour démarrer l'application Tauri
#[tokio::main]
async fn main() {
    // Configuration et exécution de l'application Tauri
    let mut app = tauri::Builder::default()
        .invoke_handler(tauri::generate_handler![
            init_db_handler,
            create_note_handler,
            read_notes_handler,
            update_note_handler,
            delete_note_handler,
            export_notes_handler,
            delete_all_notes_handler
        ])
        .run(tauri::generate_context!())
        .expect("Error running Tauri application");
}
