const { invoke } = window.__TAURI__.tauri;

let titleInputEl;
let contentInputEl;

async function fetchNotes() {
    const notes = await invoke('read_notes_handler');
    const noteList = document.getElementById('note-list');
    noteList.innerHTML = '';
    notes.forEach(note => {
        const listItem = document.createElement('li');
        listItem.textContent = `${note[1]}: ${note[2]}`;
        listItem.addEventListener('click', () => {
            titleInputEl.value = note[1];
            contentInputEl.value = note[2];
            document.getElementById('submit-btn').style.display = 'none';
            document.getElementById('update-btn').style.display = 'inline-block';
            document.getElementById('delete-btn').style.display = 'inline-block';
            document.getElementById('update-btn').onclick = () => {
                updateNote(note[0]);
            };
            document.getElementById('delete-btn').onclick = () => {
                deleteNote(note[0]);
            };
        });
        noteList.appendChild(listItem);
    });
}

async function updateNote(id) {
    const title = titleInputEl.value;
    const content = contentInputEl.value;
    await invoke('update_note_handler', { id, title, content });
    alert('Note updated successfully!');
    document.getElementById('submit-btn').style.display = 'inline-block';
    document.getElementById('update-btn').style.display = 'none';
    document.getElementById('delete-btn').style.display = 'none';
    document.getElementById('note-form').reset();
    fetchNotes(); // Met à jour la liste des notes après la modification d'une note
}

async function deleteNote(id) {
    const confirmation = confirm('Are you sure you want to delete this note?');
    if (confirmation) {
        await invoke('delete_note_handler', { id });
        alert('Note deleted successfully!');
        document.getElementById('submit-btn').style.display = 'inline-block';
        document.getElementById('update-btn').style.display = 'none';
        document.getElementById('delete-btn').style.display = 'none';
        document.getElementById('note-form').reset();
        fetchNotes(); // Met à jour la liste des notes après la suppression d'une note
    }
}

async function addNote() {
    const title = titleInputEl.value;
    const content = contentInputEl.value;
    await invoke('create_note_handler', { title, content });
    alert('Note added successfully!');
    document.getElementById('note-form').reset();
    fetchNotes(); // Met à jour la liste des notes après l'ajout d'une nouvelle note
}

async function exportNotes() {
    await invoke('export_notes_handler');
    alert('Notes exported successfully!');
}

async function deleteAllNotes() {
    const confirmation = confirm('Are you sure you want to delete all notes?');
    if (confirmation) {
        await invoke('delete_all_notes_handler');
        alert('All notes deleted successfully!');
        fetchNotes(); // Met à jour la liste des notes après la suppression de toutes les notes
    }
}

// Modifiez le gestionnaire d'événements sur le formulaire pour écouter l'événement de soumission
document.getElementById('note-form').addEventListener('submit', async (event) => {
    event.preventDefault(); // Empêche le comportement par défaut du formulaire (rechargement de la page)
    await addNote(); // Appel de la fonction pour créer une note
});

// Déplacez cet événement à l'intérieur de la fonction window.addEventListener('DOMContentLoaded', ...)
document.getElementById('delete-all-btn').addEventListener('click', deleteAllNotes);

window.addEventListener('DOMContentLoaded', () => {
    titleInputEl = document.getElementById('title');
    contentInputEl = document.getElementById('content');
    // Ajoutez cet événement pour écouter le clic sur le bouton "Add Note" (s'il est toujours nécessaire)
    document.getElementById('submit-btn').addEventListener('click', addNote);
    document.getElementById('export-btn').addEventListener('click', exportNotes);
    fetchNotes();
});
